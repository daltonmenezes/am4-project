<?php

namespace application\models;

class AppModel
{
	public function __construct()
	{
		include_once "application/storage/database/mysql/MysqlConnection.php";
		$this->connection = $connection;
	}

	public function getAllDataElements()
	{
		$this->stmt = $this->connection->prepare("SELECT id,nome,email FROM usuario");
		$this->stmt->execute();
		return $this->stmt->fetchAll();
	}

	public function save($nome, $email)
	{
		$this->stmt = $this->connection->prepare("INSERT INTO usuario (nome,email) VALUES (:nome, :email)");
		$this->stmt->bindParam(":nome",$nome);
		$this->stmt->bindParam(":email",$email);
		
		if($this->stmt->execute())
		{
			return "$nome foi cadastrado com sucesso!";
		}
		else
		{
			return "Não foi possível cadastrar $nome";
		}
	}	
}