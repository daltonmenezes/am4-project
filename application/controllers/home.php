<?php

namespace application\controllers;
class Home
{
	private function listar()
	{
		$this->model = new \application\models\Cliente();
		$this->response = $this->model->getAllDataElements();
	}

	public function getResponse()
	{
		self::listar();

		$response = $this->response;
		include_once 'application/views/home.php';
	}	
}