<?php

namespace application\controllers;

class Cadastro
{
	public function __construct($action)
	{
		if($action)
		{
			$this->view = 'application/views/resultado.php';	
		}
		else
		{
			$this->view = 'application/views/cadastrar.php';	
		}
	}

	public function incluir($var)
	{
		$this->model = new \application\models\Cliente();
		$this->response = $this->model->save($var['nome'], $var['email']);
	}

	public function getResponse()
	{
		$response = $this->response;
		include_once $this->view;
	}	
}