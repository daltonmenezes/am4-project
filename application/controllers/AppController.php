<?php

namespace application\controllers;

class AppController
{
	public function __construct()
	{
		$this->controller = $_REQUEST['controller'];
		$this->action = $_REQUEST['action'];
		$this->namespace_controller = "application\controllers\\$this->controller";
	}

	private function callRequestedControllerFromURI()
	{
		$this->controller = new $this->namespace_controller($this->action);
		
		if(method_exists($this->controller, $this->action))
		{
			$this->controller->$_REQUEST['action']($_REQUEST);
		}

		return $this->controller->getResponse();		
	}

	public function run()
	{
		self::callRequestedControllerFromURI();
	}
}