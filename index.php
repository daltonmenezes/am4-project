<?php
error_reporting(0);
include_once "bootstrap/autoload.php";

use application\controllers\AppController;

$instance = new AppController;
$instance->run();